package com.m0rb1u5.pdm_lab4_2_actividad4;

import android.content.Intent;
import android.net.Uri;
import android.provider.AlarmClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class SecondActivity extends AppCompatActivity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_second);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_second, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   public void paginaWeb(View view) {
      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse("http://www.uni.edu.pe"));
      startActivity(intent);
   }

   public void marcartelefono(View view) {
      Intent intent = new Intent(Intent.ACTION_DIAL);
      startActivity(intent);
   }

   public void llamartelefono(View view) {
      Intent intent = new Intent(Intent.ACTION_CALL);
      intent.setData(Uri.parse("tel:"+getResources().getString(R.string.tfno)));
      startActivity(intent);
   }

   public void anadircontacto(View view) {
      Intent intent = new Intent(Intent.ACTION_INSERT);
      intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
      startActivity(intent);
   }

   public void alarma(View view) {
      Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
      startActivity(intent);
   }

   public void enviaremail(View view) {
      Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
            "mailto", getResources().getString(R.string.mail), null));
      intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.subject));
      startActivity(Intent.createChooser(intent, getResources().getString(R.string.envio)));
   }

   public void camara(View view) {
      Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      startActivity(intent);
   }

   public void videocamara(View view) {
      Intent intent = new Intent(MediaStore.INTENT_ACTION_VIDEO_CAMERA);
      startActivity(intent);
   }

   public void gps(View view) {
      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse(getResources().getString(R.string.coord)));
      startActivity(intent);
   }

   public void compartirtexto(View view) {
      Intent intent = new Intent();
      intent.setAction(Intent.ACTION_SEND);
      intent.putExtra(Intent.EXTRA_TEXT,getResources().getString(R.string.text));
      intent.setType("text/plain");
      startActivity(intent);
   }
}
